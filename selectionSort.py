# -*- coding: utf-8 -*-
"""
Created on Sat Oct 22 17:14:55 2022

@author: Admin
"""
import csv

rat,m,y,g,d = [],[],[],[],[]
with open('DataFile.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
for i in data:
    rat.append(float(i[3]))
    m.append(i[0])
    y.append(i[1])
    g.append(i[2])
    d.append(i[4])
for i in range(len(data)):
    data[i][3] = float(data[i][3])

# Counting sort in Python programming

# Selection sort in Python


def selectionSort(array, size):
   
    for step in range(size):
        min_idx = step

        for i in range(step + 1, size):
         
            # to sort in descending order, change > to < in this line
            # select the minimum element in each loop
            if array[i][3] < array[min_idx][3]:
                min_idx = i
         
        # put min at the correct position
        (array[step], array[min_idx]) = (array[min_idx], array[step])


#data = [-2, 45, 0, 11, -9]
size = len(data)
selectionSort(data, size)
print('Sorted Array in Ascending Order:')
print(data)