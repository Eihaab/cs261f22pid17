# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 21:11:29 2022

@author: Admin
"""
import csv
import matplotlib.pyplot as plt
import pandas as pd

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
rank,name,year,genre,pg,rat,meta,direc = [],[],[],[],[],[],[],[]
for i in data:
    i[2] = i[2].strip('(IV)')
    i[2] = i[2].strip('(I)')
    i[2] = i[2].strip('(')
    i[2] = i[2].strip(' (')
    i[2] = i[2].strip(')')
    i[2] = i[2].strip('-')
for i in data:
    rank.append(int(i[0]))
    name.append(i[1])
    year.append(int(i[2]))
    genre.append(i[3])
    pg.append(i[4])
    rat.append(float(i[5]))
    meta.append(int(i[6]))
    direc.append(i[7])


# Using counting sort to sort the elements in the basis of significant places
def countingSort(rat1, place):
    size = len(rat1)
    o0 = [0]*size
    o1 = [0]*size
    o2 = [0]*size
    o3 = [0]*size
    o4 = [0]*size
    o5 = [0]*size
    o6 = [0]*size
    o7 = [0]*size
    count = [0] * 10

    # Calculate count of elements
    for i in range(0, size):
        index = int(rat1[i] // place)
        count[index % 10] += 1

    # Calculate cumulative count
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Place the elements in sorted order
    i = size - 1
    while i >= 0:
        index = int(rat1[i] // place)
        o0[count[index % 10] - 1] = rank[i]
        o1[count[index % 10] - 1] = name[i]
        o2[count[index % 10] - 1] = year[i]
        o3[count[index % 10] - 1] = genre[i]
        o4[count[index % 10] - 1] = pg[i]
        o5[count[index % 10] - 1] = rat[i]
        o6[count[index % 10] - 1] = meta[i]
        o7[count[index % 10] - 1] = direc[i]
        count[index % 10] -= 1
        i -= 1

    for i in range(0, size):
        rank[i] = o0[i]
        name[i] = o1[i]
        year[i] = o2[i]
        genre[i] = o3[i]
        pg[i] = o4[i]
        rat[i] = o5[i]
        meta[i] = o6[i]
        direc[i] = o7[i]

# Main function to implement radix sort
def radixSortOnRank(val1):
    # Get maximum element
    max_element = max(val1)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while max_element // place > 0:
        countingSort(val1, place)
        place *= 10
    val = []
    for i in range(len(rank)):
        newD = []
        newD.append(rank[i])
        newD.append(name[i])
        newD.append(year[i])
        newD.append(genre[i])
        newD.append(pg[i])
        newD.append(rat[i])
        newD.append(meta[i])
        newD.append(direc[i])
        val.append(newD)
    data = val
    print(data)
 
def radixSortOnYear(val1):
    # Get maximum element
    max_element = max(val1)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while max_element // place > 0:
        countingSort(val1, place)
        place *= 10
    val = []
    for i in range(len(rank)):
        newD = []
        newD.append(rank[i])
        newD.append(name[i])
        newD.append(year[i])
        newD.append(genre[i])
        newD.append(pg[i])
        newD.append(rat[i])
        newD.append(meta[i])
        newD.append(direc[i])
        val.append(newD)
    data = val
    print(data)

radixSortOnRank(rank)
 
def radixSortOnIMBD(val1):
    # Get maximum element
    max_element = max(val1)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while max_element // place > 0:
        countingSort(val1, place)
        place *= 10
    val = []
    for i in range(len(rank)):
        newD = []
        newD.append(rank[i])
        newD.append(name[i])
        newD.append(year[i])
        newD.append(genre[i])
        newD.append(pg[i])
        newD.append(rat[i])
        newD.append(meta[i])
        newD.append(direc[i])
        val.append(newD)
    data = val
    print(data)

 
def radixSortOnMetaScore(val1):
    # Get maximum element
    max_element = max(val1)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while max_element // place > 0:
        countingSort(val1, place)
        place *= 10
    val = []
    for i in range(len(rank)):
        newD = []
        newD.append(rank[i])
        newD.append(name[i])
        newD.append(year[i])
        newD.append(genre[i])
        newD.append(pg[i])
        newD.append(rat[i])
        newD.append(meta[i])
        newD.append(direc[i])
        val.append(newD)
    data = val
    print(data)


