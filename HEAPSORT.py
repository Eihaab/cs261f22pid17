# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 22:19:18 2022

@author: Admin
"""
import csv

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
# Heap Sort in python


def heapify(arr, n, i):
      # Find largest among root and children
      largest = i
      l = 2 * i + 1
      r = 2 * i + 2
  
      if l < n and arr[i][5] < arr[l][5]:
          largest = l
  
      if r < n and arr[largest][5] < arr[r][5]:
          largest = r
  
      # If root is not largest, swap with largest and continue heapifying
      if largest != i:
          arr[i], arr[largest] = arr[largest], arr[i]
          heapify(arr, n, largest)
  
  
def heapSort(arr):
      n = len(arr)
  
      # Build max heap
      for i in range(n//2, -1, -1):
          heapify(arr, n, i)
  
      for i in range(n-1, 0, -1):
          # Swap
          arr[i], arr[0] = arr[0], arr[i]
  
          # Heapify root element
          heapify(arr, i, 0)
  
  
arr = [1, 12, 9, 5, 6, 10]
heapSort(data)
n = len(arr)
print("Sorted array is")
print(data)
  