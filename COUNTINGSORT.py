# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 20:41:10 2022

@author: Admin
"""
import csv

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
rank,name,year,genre,pg,rat,meta,direc = [],[],[],[],[],[],[],[]
for i in data:
    i[2] = i[2].strip('(IV)')
    i[2] = i[2].strip('(I)')
    i[2] = i[2].strip('(')
    i[2] = i[2].strip(' (')
    i[2] = i[2].strip(')')
    i[2] = i[2].strip('-')
for i in data:
    rank.append(int(i[0]))
    name.append(i[1])
    year.append(int(i[2]))
    genre.append(i[3])
    pg.append(i[4])
    rat.append(float(i[5]))
    meta.append(int(i[6]))
    direc.append(i[7])
# Counting sort in Python programming
print(len(rank))

def countingSortOnRat(rat1):
    
    size = len(rat1)
    o0 = [0] * size
    o1,o2,o3,o4,o5,o6,o7=[0]* size,[0]* size,[0]* size,[0]* size,[0]* size,[0]* size,[0]* size
    # Initialize count rat
    count = [0] * 10

    # Store the count of each elements in count rat
    for i in range(0, size):
        count[int(rat1[i])] += 1

    # Store the cummulative count
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Find the index of each element of the original rat in count rat
    # place the elements in output rat
    i = size - 1
    while i >= 0:
        o0[count[int(rat1[i])] - 1] = rank[i]
        o1[count[int(rat1[i])] - 1] = name[i]
        o2[count[int(rat1[i])] - 1] = year[i]
        o3[count[int(rat1[i])] - 1] = genre[i]
        o4[count[int(rat1[i])] - 1] = pg[i]
        o5[count[int(rat1[i])] - 1] = rat[i]
        o6[count[int(rat1[i])] - 1] = meta[i]
        o7[count[int(rat1[i])] - 1] = direc[i]
        count[int(rat1[i])] -= 1
        i -= 1

    # Copy the sorted elements into original rat
    for i in range(0, size):
        rank[i] = o0[i]
        name[i] = o1[i]
        year[i] = o2[i]
        genre[i] = o3[i]
        pg[i] = o4[i]
        rat[i] = o5[i]
        meta[i] = o6[i]
        direc[i] = o7[i]
        
    val = []
    for i in range(len(rank)):
        newD = []
        newD.append(rank[i])
        newD.append(name[i])
        newD.append(year[i])
        newD.append(genre[i])
        newD.append(pg[i])
        newD.append(rat[i])
        newD.append(meta[i])
        newD.append(direc[i])
        val.append(newD)
    data = val
    print(data)
countingSortOnRat(rat)
