# -*- coding: utf-8 -*-
"""
Created on Sat Oct 15 09:14:10 2022

@author: Admin
"""

import matplotlib.pyplot as plt
import pandas as pd

path=r'C:\SMESTER3\DSA\Week4\Lab4\Part4\DataFile.csv'
df=pd.read_csv(path)

list1,list2 = [],[]
movie,year,genre,rating,direction = [],[],[],[],[]
#print(df.head())
m=df['Movie Name']
y=df['Year']
g=df['Genre']
r=df['Rating']
d=df['Direction']
 
length = len(r)
for i in range(1,length):
    c1 = r[i]
    c2 = m[i]
    c3 = y[i]
    c4 = g[i]
    c5 = d[i]
    j = i-1
    while(j >= 0 and r[j] > c1):
        r[j + 1] = r[j]
        m[j + 1] = m[j]
        y[j + 1] = y[j]
        g[j + 1] = g[j]
        d[j + 1] = d[j]
        j = j-1
    r[j + 1] = c1
    m[j + 1] = c2
    y[j + 1] = c3
    g[j + 1] = c4
    d[j + 1] = c5

print(m)
print(r)

