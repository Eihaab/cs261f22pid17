from bs4 import BeautifulSoup
import requests
# import re
import pandas as pd

try:
    for i in range(51,42000,50):
        
        source=requests.get(f'https://www.imdb.com/search/title/?title_type=feature&release_date=1900-01-01,2030-12-31&start={i}&ref_=adv_nxt')
   
        soup=BeautifulSoup(source.text,'html.parser')
        Data=soup.find('body').find_all('div', class_="lister-item mode-advanced") 
    
    
        name=[]
        Year=[]
        Genre=[]
        ge=[]
        Rating=[]
        ra=[]
        Direction=[]
    
        for dt in Data:
            rank=dt.find('div',class_="lister-item-content").span.text
            Mname=dt.find('div',class_="lister-item-content").a.text
            year=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
        
            
            if dt.find('div',class_="lister-item-content"):
                genre=dt.find('p',class_="text-muted").find('span',class_="genre").text
            else:
                genre="NA"
        
            if dt.find('div',class_="ratings-bar"):
                rating=dt.find('div',class_="inline-block ratings-imdb-rating").strong.text
            else:
                rating="NA"
            
            direction=dt.find('div',class_="lister-item-content").find('p',class_="").a.text
        
            if dt.find('div',class_="lister-item-content"):   
                viewer=dt.find('span',class_="certificate").text
            else:
                viewer="NA"
            
            if dt.find('div',class_="ratings-bar"):
                score=dt.find('div',class_="inline-block ratings-metascore").span.text
            else:
                score="NA"
        
        
        
        # story=dt.find('div',class_="lister-item-content").find('p',class_="text-muted").text
        # star=dt.find('div',class_="lister-item-content").find('p',class_="").text
      
        # print(rank)
        # print(Mname)
        # print(year)  
        # print(genre)
        # print(rating)
        # print(viewer)
        # print(direction)
        # print(score)
        # print('NEW')
        
        
        # print(story)
        # print(star)
        
            
            name.append(Mname)
            Year.append(year)
            ge.append(genre)
            ra.append(rating)
            Direction.append(direction) 
        
        Genre=list(map(str.strip,ge))
        Rating=list(map(str.strip,ra))

    # print(Genre)
    # print(Rating)
 
    # print(name)
    # print(Year)  
    # # print(Rating)
  
        df = pd.DataFrame({'Movie Name':name,'Year':Year,'Genre':Genre,'Rating':Rating,'Direction':Direction})
        df.to_csv('DataFile.csv',index=False,encoding='utf-8')
         
        
    
    
    
except Exception as e:
    print(e)

