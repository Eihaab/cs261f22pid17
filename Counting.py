# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 20:41:10 2022

@author: Admin
"""
import csv

rat,m,y,g,d = [],[],[],[],[]
with open('DataFile.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
for i in data:
    rat.append(float(i[3]))
    m.append(i[0])
    y.append(i[1])
    g.append(i[2])
    d.append(i[4])
# Counting sort in Python programming


def countingSort():
    
    size = len(rat)
    output = [0] * size
    o1,o2,o3,o4=[0]* size,[0]* size,[0]* size,[0]* size
    # Initialize count rat
    count = [0] * 10

    # Store the count of each elements in count rat
    for i in range(0, size):
        count[int(rat[i])] += 1

    # Store the cummulative count
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Find the index of each element of the original rat in count rat
    # place the elements in output rat
    i = size - 1
    while i >= 0:
        output[count[int(rat[i])] - 1] = rat[i]
        o1[count[int(rat[i])] - 1] = m[i]
        o2[count[int(rat[i])] - 1] = y[i]
        o3[count[int(rat[i])] - 1] = g[i]
        o4[count[int(rat[i])] - 1] = d[i]
        count[int(rat[i])] -= 1
        i -= 1

    # Copy the sorted elements into original rat
    for i in range(0, size):
        rat[i] = output[i]
        m[i] = o1[i]
        y[i] = o2[i]
        g[i] = o3[i]
        d[i] = o4[i]
        
    data1 = []
    for i in range(len(rat)):
        newD = []
        newD.append(m[i])
        newD.append(y[i])
        newD.append(g[i])
        newD.append(str(rat[i]))
        newD.append(d[i])
        data1.append(newD)
    print(data1)
data = [4, 2, 2, 8, 3, 3, 1]
countingSort()
print("Sorted rat in Ascending Order: ")