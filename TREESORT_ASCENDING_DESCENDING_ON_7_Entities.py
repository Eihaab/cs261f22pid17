import csv
import matplotlib.pyplot as plt
import pandas as pd

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
rank,name,year,genre,pg,rat,meta,direc = [],[],[],[],[],[],[],[]
for i in data:
    i[2] = i[2].strip('(IV)')
    i[2] = i[2].strip('(I)')
    i[2] = i[2].strip('(')
    i[2] = i[2].strip(' (')
    i[2] = i[2].strip(')')
    i[2] = i[2].strip('-')
for i in data:
    rank.append(int(i[0]))
    name.append(i[1])
    year.append(int(i[2]))
    genre.append(i[3])
    pg.append(i[4])
    rat.append(float(i[5]))
    meta.append(int(i[6]))
    direc.append(i[7])
# Python3 program to 
# implement Tree Sort
  
# Class containing left and
# right child of current 
# node and key value
class Node:
  
  def __init__(self,item = []):
    self.key = item
    self.left,self.right = None,None
  
  
# Root of BST
root = Node()
  
root = None
  
# This method mainly
# calls insertRec()
def insert(key):
  global root
  root = insertRec(root, key)
  
# A recursive function to 
# insert a new key in BST
def insertRec(root, key):
  
  # If the tree is empty,
  # return a new node
  
  if (root == None):
    root = Node(key)
    return root
  
  # Otherwise, recur
  # down the tree 
  # To sorting on other entity just change the key index of data and data will be sorted accordingly
  if (key[0]< root.key[0]):
    root.left = insertRec(root.left, key)
  elif (key[0]> root.key[0]):
    root.right = insertRec(root.right, key)
  
  # return the root
  return root
  
# A function to do 
# inorder traversal of BST
def inorderRec(root):
  if (root != None):
    inorderRec(root.left)
   #print(root.key ,end = " ")
    rat1.append(root.key)
    inorderRec(root.right)


def treeins(rat):
  for i in range(len(rat)):
    insert(rat[i])
  
  
# Driver Code
rat1 = []
treeins(data)
inorderRec(root)
data1 = []
for i in range(len(rat1)):
      newD = []
      newD.append(rat1[i][0])
      newD.append(rat1[i][1])
      newD.append(rat1[i][2])
      newD.append(rat1[i][3])
      newD.append(rat1[i][4])
      newD.append(rat1[i][5])
      newD.append(rat1[i][6])
      newD.append(rat1[i][7])
      data1.append(newD)
print(data1)
# This code is contributed by shinjanpatra\