# -*- coding: utf-8 -*-
"""
Created on Tue Oct 25 10:53:40 2022

@author: Admin
"""

import matplotlib.pyplot as plt
import pandas as pd
import csv

with open('products.csv', 'r', encoding="utf-8", newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
#for i in data:
 #   float(i[0])
#float(data[0][0])
for i in data:
    i[2] = i[2].strip('(IV)')
    i[2] = i[2].strip('(I)')
    i[2] = i[2].strip('(')
    i[2] = i[2].strip(' (')
    i[2] = i[2].strip(')')
    i[2] = i[2].strip('-')
    print(i[2])

def bubbleSortWithDirectionA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][7] > data[j + 1][7]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
def bubbleSortWithDirectionD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][7] < data[j + 1][7]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithGenreA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][3] > data[j + 1][3]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
def bubbleSortWithGenreD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][3] < data[j + 1][3]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithIMBDA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][5] > data[j + 1][5]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithIMBDD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][5] < data[j + 1][5]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
def bubbleSortWithMetaA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][6] > data[j + 1][6]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
            
def bubbleSortWithMetaD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][6] < data[j + 1][6]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
                
def bubbleSortWithMovieA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][1] > data[j + 1][1]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithMovieD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][1] < data[j + 1][1]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
                
def bubbleSortWithPGA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][4] > data[j + 1][4]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithPGD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if data[j][4] < data[j + 1][4]:

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithRankA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if float(data[j][0]) > float(data[j + 1][0]):

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp
                
def bubbleSortWithRankD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if float(data[j][0]) < float(data[j + 1][0]):

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithYearA(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if float(data[j][2]) > float(data[j + 1][2]):

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp

def bubbleSortWithYearD(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if float(data[j][2]) < float(data[j + 1][2]):

                # swapping elements if elements
                # are not in the intended order
                temp = data[j]
                data[j] = data[j+1]
                data[j+1] = temp