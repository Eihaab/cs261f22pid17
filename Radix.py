# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 11:54:53 2022

@author: Admin
"""
import csv
import matplotlib.pyplot as plt
import pandas as pd
path=r'C:\SMESTER3\WebScrapping\DataFile.csv'
df=pd.read_csv(path)

rat,m,y,g,d = [],[],[],[],[]
with open('DataFile.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
for i in data:
    rat.append(float(i[3]))
    m.append(i[0])
    y.append(i[1])
    g.append(i[2])
    d.append(i[4])


# Using counting sort to sort the elements in the basis of significant places
def countingSort(rat, place):
    size = len(rat)
    output = [0] * size
    o1 = [0]*size
    o2 = [0]*size
    o3 = [0]*size
    o4 = [0]*size
    count = [0] * 10

    # Calculate count of elements
    for i in range(0, size):
        index = int(rat[i] // place)
        count[index % 10] += 1

    # Calculate cumulative count
    for i in range(1, 10):
        count[i] += count[i - 1]

    # Place the elements in sorted order
    i = size - 1
    while i >= 0:
        index = int(rat[i] // place)
        output[count[index % 10] - 1] = rat[i]
        o1[count[index % 10] - 1] = m[i]
        o2[count[index % 10] - 1] = y[i]
        o3[count[index % 10] - 1] = g[i]
        o4[count[index % 10] - 1] = d[i]
        count[index % 10] -= 1
        i -= 1

    for i in range(0, size):
        rat[i] = output[i]
        m[i] = o1[i]
        y[i] = o2[i]
        g[i] = o3[i]
        d[i] = o4[i]

# Main function to implement radix sort
def radixSort(rat):
    # Get maximum element
    max_element = max(rat)

    # Apply counting sort to sort elements based on place value.
    place = 1
    while max_element // place > 0:
        countingSort(rat, place)
        place *= 10


radixSort(rat)
data1 = []
for i in range(len(rat)):
    newD = []
    newD.append(m[i])
    newD.append(y[i])
    newD.append(g[i])
    newD.append(str(rat[i]))
    newD.append(d[i])
    data1.append(newD)
data = data1
print(data1)


