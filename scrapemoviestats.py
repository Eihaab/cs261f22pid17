from cgitb import strong
from turtle import title
from bs4 import BeautifulSoup
import requests, openpyxl

excel = openpyxl.Workbook()
print(excel.sheetnames)
sheet = excel.active
sheet.title = 'Movies_Info'
print(excel.sheetnames)
sheet.append(['No.','Movie','Year','PG-Rating','Genre','IMDB-Rating','Metascore','Director and Stars'])

try:
    for i in range(51,10000,50):
        source=requests.get(f'https://www.imdb.com/search/title/?title_type=feature&release_date=1900-01-01,2030-12-31&start={i}&ref_=adv_nxt')
    # source = requests.get('https://www.imdb.com/search/title/?title_type=feature&release_date=1900-01-01,2030-12-31&ref_=adv_prv')
        source.raise_for_status()

        soup = BeautifulSoup(source.text,'html.parser')
    
        movies = soup.find('div' , class_ = "lister-list").find_all('div', class_ = "lister-item mode-advanced")
    
        for movie in movies:

            name = movie.find('h3', class_ = "lister-item-header").a.text

           
            rank = movie.span.text

            year = movie.find('span', class_ = "lister-item-year text-muted unbold").text

            if(movie.find('div', class_ = "inline-block ratings-imdb-rating")):
                rating = movie.find('div', class_ = "inline-block ratings-imdb-rating").strong.text
            else:
                rating = 0

            if(movie.find('div', class_ = "inline-block ratings-metascore")):
                metascore = movie.find('div', class_ = "inline-block ratings-metascore").span.text
            else:
                metascore = 0

            if(movie.find('span', class_ = "certificate")):
                certi = movie.find('span', class_ = "certificate").text
            else:
                certi = 'E'

            if(movie.find('span', class_ = "genre")):
                Genre = movie.find('span', class_ = "genre").text
            else:
                Genre = 'NA'
            
            if(movie.find('p', class_= "")):
                director = movie.find('p', class_= "").text
            else:
                director = 'NA'
        
            print(rank,name,year,certi,Genre,rating,metascore,director)
            sheet.append([rank,name,year,certi,Genre,rating,metascore,director])

except Exception as e:
    print(e)
excel.save('IMDB Movies list.xlsx')