import csv 
import pandas as pd
from funcs import MergeSort
from funcs import SaveArray


def load_data(filename):
    datalist=[]
    with open(filename,encoding='utf-8') as rank:
        rank_data=csv.reader(rank,delimiter=',')
        next(rank_data)
        for row in rank_data:
            datalist.append(row[0])
        return datalist
    
    
data=[]
new_list=load_data('MergeSortData.csv')
for row in new_list:
    data.append(int(row))
# print(type(int(new_list[0])))
MergeSort(data)
# print(data)
SaveArray(data, "MergeSortRank.csv")