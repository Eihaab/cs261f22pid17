from cgitb import strong
from bs4 import BeautifulSoup
import requests
# import re
import pandas as pd

try:
    name=[]
    Year=[]
    Genre=[]
    ge=[]
    Rating=[]
    ra=[]
    Direction=[]
    start = 1
    while(start < 406359):
        url = "https://www.imdb.com/search/title/?title_type=feature&release_date=1850-01-01,2021-12-31&start="+str(start)+"&ref_=adv_nxt"
        source=requests.get(url)
        print(url)
        soup=BeautifulSoup(source.text,'html.parser')
        Data=soup.find('body').find_all('div', class_="lister-item mode-advanced") 
        for dt in Data:
            #print(type(dt.find('div',class_="lister-item-content").find('div',class_="inline-block ratings-imdb-rating")))
            if(dt.find('div',class_="lister-item-content").a.text is not None):
                Mname=dt.find('div',class_="lister-item-content").a.text
            else:
                Mname = "shutter Island"

            if(dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text is not None):
                year1=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
                #year1 = int(year1)
                year = year1
            else:
                year = 2019
            
            if(dt.find('div',class_="lister-item-content").find('span',class_="genre").text is not None):
                genre=dt.find('div',class_="lister-item-content").find('span',class_="genre").text
            else:
                genre = "crime,drama"               

            if(dt.find('div',class_="lister-item-content").find('div',class_="inline-block ratings-imdb-rating").strong.text is not None):
                rating = dt.find('div',class_="lister-item-content").find('div',class_="inline-block ratings-imdb-rating").strong.text
            else:
                rating = 4
            #rating=dt.find('div',class_="lister-item-content").find('div',class_="inline-block ratings-imdb-rating").text
            
            if(dt.find('div',class_="lister-item-content").find('p',class_="").a.text is not None):
                direction=dt.find('div',class_="lister-item-content").find('p',class_="").a.text
            else:
                direction = "James cameron"
           # story=dt.find('div',class_="lister-item-content").find('p',class_="text-muted").text
           # star=dt.find('div',class_="lister-item-content").find('p',class_="").text
           # print(Mname)
           # print(year)  
           # print(genre)
           # print(rating)
           #print(story)
           # print(direction)
           # print(star)    
            name.append(Mname)
            Year.append(year)
            ge.append(genre)
            Rating.append(rating)
            Direction.append(direction)
        start = start + 50
        
    Genre=list(map(str.strip,ge))
    #Rating=list(map(int.strip,ra))

    # print(Genre)
    # print(Rating)
 
    # print(name)
    # # print(Year)  
    # print(Rating)
  
    df = pd.DataFrame({'Movie Name':name,'Year':Year,'Genre':Genre,'Rating':Rating,'Direction':Direction})
    df.to_csv('DataFile.csv',index=False,encoding='utf-8')
         

    
    
except Exception as e:
    print(e)

# 1-100. https://www.imdb.com/search/title/?count=100&groups=top_1000&sort=user_rating'


# https://www.imdb.com/search/title/?groups=top_1000&sort=user_rating,desc&count=100&start=101&ref_=adv_nxt