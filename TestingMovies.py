import sys
from PyQt5.uic import loadUi
import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
from tkinter import*
from tkinter import ttk,messagebox

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        loadUi("MoviesProject2.ui",self)
        self.MoviesTable.setColumnWidth(0, 250)
        self.MoviesTable.setColumnWidth(1, 100)
        self.MoviesTable.setColumnWidth(2, 350)
        self.loaddata()
        
    def loaddata(self):
        with open('DataFile111.csv', 'r',encoding="utf-8",newline="") as fileInput:
            data = list(csv.reader(fileInput))

        tablerow=0
        self.MoviesTable.setRowCount(len(data))
        for row in data:
            self.MoviesTable.setItem(tablerow, 0, QtWidgets.QTableWidgetItem(row[0]))
            self.MoviesTable.setItem(tablerow, 1, QtWidgets.QTableWidgetItem(row[1]))
            self.MoviesTable.setItem(tablerow, 2, QtWidgets.QTableWidgetItem(row[2]))
            self.MoviesTable.setItem(tablerow, 3, QtWidgets.QTableWidgetItem(row[3]))
            self.MoviesTable.setItem(tablerow, 4, QtWidgets.QTableWidgetItem(row[4]))

            tablerow+=1
# main
app = QApplication(sys.argv)
mainwindow = MainWindow()
widget = QtWidgets.QStackedWidget()
widget.addWidget(mainwindow)
widget.setFixedHeight(500)
widget.setFixedWidth(800)
widget.show()
try:
    sys.exit(app.exec_())
except:
    print("Exiting")