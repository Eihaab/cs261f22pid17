
import time

from PyQt5.uic import loadUi
from PyQt5.QtWidgets import*
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
from bs4 import BeautifulSoup
import requests, openpyxl
from urllib.parse import urljoin



# def scraping_pause_resume():
    # try:
    #     time.sleep(1)
    #     Start_Scrapping()
    # except KeyboardInterrupt:
    #     try:
    #         response = input()
    #         if response == 'quit':
    #             break
    #         print ('Resuming...')
    #     except KeyboardInterrupt:
    #         print ('Resuming...')
    #         continue

def Start_Scrapping():
    try:
        url='https://www.imdb.com/search/title/?title_type=feature&release_date=2015-01-01,2015-12-31&after=WzUxOTE1LCJ0dDkxNzYyNjAiLDEwMTAxXQ%3D%3D&ref_=adv_nxt'
        
        Rank=[]
        Mname=[]
        Year=[]
        Genre=[]
        Restrict=[]
        Rating=[]
        Metascore=[]
        Director=[]
        
        while True:
            # print(url)
            source=requests.get(url)
            soup=BeautifulSoup(source.text,'html.parser')
            nextpage=soup.select_one('div.desc>a')
            if nextpage:
                nexturl=nextpage.get('href')
                url=urljoin(url, nexturl)
            else:
                break 
        
            source=requests.get(url)
            source.raise_for_status()

            soup = BeautifulSoup(source.text,'html.parser')
        
            movies = soup.find('div' , class_ = "lister-list").find_all('div', class_ = "lister-item mode-advanced")
            
            for dt in movies:
                
                if dt.find('div',class_="lister-item-content"):    
                    rank=dt.find('div',class_="lister-item-content").span.text
                    mname=dt.find('div',class_="lister-item-content").a.text
                    year=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
                else:
                    rank,Mname,year="NA"
                    
                if dt.find('p',class_="text-muted"):
                    if dt.find('span',class_="genre"):
                        genre=dt.find('span',class_="genre").text
                else:
                    genre="NA"

                if dt.find('span', class_ = "certificate"):
                    restrict = dt.find('span', class_ = "certificate").text
                else:
                    restrict = 'NA'

                if dt.find('div',class_="inline-block ratings-imdb-rating"):
                    rating=dt.find('strong').text
                else:
                    rating="NA"

                if(dt.find('div', class_ = "inline-block ratings-metascore")):
                    metascore = dt.find('div', class_ = "inline-block ratings-metascore").span.text
                else:
                    metascore = 0
                
                            
                if(dt.find('p', class_= "").find('a')):
                    director = dt.find('p',class_="").a.text
                else:
                    director = 'NA'
           
                    
                print(rank,Mname,year,genre,restrict,rating,metascore,director)
                
                
                Rank.append(rank)
                Mname.append(mname)
                Year.append(year)
                Genre.append(genre.strip())
                Restrict.append(restrict)
                Rating.append(rating)
                Metascore.append(metascore)
                Director.append(director)
                
            df = pd.DataFrame({'Rank':Rank,'Movies Name':Mname,'Year':Year,'Genre':Genre,'PG-Rating':Restrict,'Rating':Rating,'MetaScore':Metascore,'Director':Director}) 
            df.to_csv('Movies_data.csv', index=False, encoding='utf-8')
    except Exception as e:
        print(e)
    

