
from bs4 import BeautifulSoup
import requests, openpyxl
from urllib.parse import urljoin

excel = openpyxl.Workbook()
print(excel.sheetnames)
sheet = excel.active
sheet.title = 'Movies_Info'
print(excel.sheetnames)
sheet.append(['No.','Movie','Year','PG-Rating','Genre','IMDB-Rating','Metascore','Director'])

try:
    url='https://www.imdb.com/search/title/?title_type=feature&release_date=1950-01-01,2000-12-31&after=WzY0MjY1LCJ0dDAxODE5NDciLDM3MDUxXQ%3D%3D&ref_=adv_nxt'
    while True:
        # print(url)
        source=requests.get(url)
        soup=BeautifulSoup(source.text,'html.parser')
        nextpage=soup.select_one('div.desc>a')
        if nextpage:
            nexturl=nextpage.get('href')
            url=urljoin(url, nexturl)
        else:
            break 
    
        source=requests.get(url)
        source.raise_for_status()

        soup = BeautifulSoup(source.text,'html.parser')
    
        movies = soup.find('div' , class_ = "lister-list").find_all('div', class_ = "lister-item mode-advanced")
        
        for dt in movies:
            
            if dt.find('div',class_="lister-item-content"):    
                rank=dt.find('div',class_="lister-item-content").span.text
                Mname=dt.find('div',class_="lister-item-content").a.text
                year=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
            else:
                rank,Mname,year="NA"
                
            if dt.find('p',class_="text-muted"):
                if dt.find('span',class_="genre"):
                    genre=dt.find('span',class_="genre").text
            else:
                genre="NA"

            if dt.find('span', class_ = "certificate"):
                restrict = dt.find('span', class_ = "certificate").text
            else:
                restrict = 'NA'

            if dt.find('div',class_="inline-block ratings-imdb-rating"):
                rating=dt.find('strong').text
            else:
                rating="NA"

            if(dt.find('div', class_ = "inline-block ratings-metascore")):
                metascore = dt.find('div', class_ = "inline-block ratings-metascore").span.text
            else:
                metascore = 0
            
                        
            if(dt.find('p', class_= "").find('a')):
                director = dt.find('p',class_="").a.text
            else:
                director = 'NA'
       
                
            print(rank,Mname,year,genre,restrict,rating,metascore,director)
            sheet.append([rank,Mname,year,genre,restrict,rating,metascore,director])

except Exception as e:
    print(e)
excel.save('IMDB-Movies-list Part2.csv')