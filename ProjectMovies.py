import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import*
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
from openpyxl import load_workbook
import pandas as pd
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog, QApplication
import sqlite3
from bs4 import BeautifulSoup
import requests, openpyxl
from urllib.parse import urljoin
import time

from PyQt5.QtCore import (
    Qt, QObject, pyqtSignal, pyqtSlot, QRunnable, QThreadPool
)

#-----Welcome page class---
class welcomepg(QMainWindow):
    def __init__(self):
        super(welcomepg, self).__init__()
        loadUi("welcomepg.ui",self)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.Next.clicked.connect(self.Nextpg)
    #---Function to go to next page---    
    def Nextpg(self):
        Pg1 = pg1()
        widget.addWidget(Pg1)
        widget.setCurrentIndex(widget.currentIndex()+1)
    #----Scrapping page class----
    
class WorkerSignals(QObject):
    progress = pyqtSignal(int)


class JobRunner(QRunnable):

    signals = WorkerSignals()
    
    def __init__(self):
        super().__init__()

        self.is_paused = True
        self.is_killed = False

    @pyqtSlot()
    def run(self):
        try:
            url='https://www.imdb.com/search/title/?title_type=feature&release_date=2015-01-01,2015-12-31&after=WzUxOTE1LCJ0dDkxNzYyNjAiLDEwMTAxXQ%3D%3D&ref_=adv_nxt'
            
            Rank=[]
            Mname=[]
            Year=[]
            Genre=[]
            Restrict=[]
            Rating=[]
            Metascore=[]
            Director=[]
            
            while True :
                # print(url)
                source=requests.get(url)
                soup=BeautifulSoup(source.text,'html.parser')
                nextpage=soup.select_one('div.desc>a')
                if nextpage:
                    nexturl=nextpage.get('href')
                    url=urljoin(url, nexturl)
                else:
                    break 
            
                source=requests.get(url)
                source.raise_for_status()

                soup = BeautifulSoup(source.text,'html.parser')
            
                movies = soup.find('div' , class_ = "lister-list").find_all('div', class_ = "lister-item mode-advanced")
                
                
                while self.is_paused:
                    time.sleep(0)
                for dt in movies:
                        
                    if dt.find('div',class_="lister-item-content"):    
                        rank=dt.find('div',class_="lister-item-content").span.text
                        mname=dt.find('div',class_="lister-item-content").a.text
                        year=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
                    else:
                        rank,Mname,year="NA"
                        
                    if dt.find('p',class_="text-muted"):
                        if dt.find('span',class_="genre"):
                            genre=dt.find('span',class_="genre").text
                    else:
                        genre="NA"

                    if dt.find('span', class_ = "certificate"):
                        restrict = dt.find('span', class_ = "certificate").text
                    else:
                        restrict = 'NA'
                        
                    if dt.find('div',class_="inline-block ratings-imdb-rating"):
                        rating=dt.find('strong').text
                    else:
                        rating="0.0"

                    if(dt.find('div', class_ = "inline-block ratings-metascore")):
                        metascore = dt.find('div', class_ = "inline-block ratings-metascore").span.text
                    else:
                        metascore = 0
                    
                                
                    if(dt.find('p', class_= "").find('a')):
                        director = dt.find('p',class_="").a.text
                    else:
                        director = 'NA'
               
                        
                    print(rank,Mname,year,genre,restrict,rating,metascore,director)
                    
                    
                    Rank.append(rank.replace(',', ''))
                    Mname.append(mname)
                    year = year.strip('(IV)')
                    year = year.strip('(I)')
                    year = year.strip('(')
                    year = year.strip(' (')
                    year = year.strip(')')
                    year = year.strip('-')
                    Year.append(year)
                    Genre.append(genre.strip())
                    Restrict.append(restrict)
                    Rating.append(rating)
                    Metascore.append(metascore)
                    Director.append(director)
                    
                df = pd.DataFrame({'Rank':Rank,'Movies Name':Mname,'Year':Year,'Genre':Genre,'PG-Rating':Restrict,'Rating':Rating,'MetaScore':Metascore,'Director':Director}) 
                df.to_csv('products.csv', index=False, encoding='utf-8')
        except Exception as e:
            print(e)
            
    def pause(self):
        self.is_paused = True

    def resume(self):
        self.is_paused = False

    def kill(self):
        self.is_killed = True
        
class pg1(QMainWindow):
    def __init__(self):
        super(pg1, self).__init__()
        loadUi("Scraping_UI.ui",self)
        
        # Thread runner
        self.threadpool = QThreadPool()
        
        # Create a runner
        self.runner = JobRunner()
        self.runner.signals.progress.connect(self.update_progress)
        self.threadpool.start(self.runner)
        
        self.scrap.clicked.connect(self.runner.resume)
        self.pause.clicked.connect(self.runner.pause)
        self.stop.clicked.connect(self.gotoNextScreen)
        #self.pause.clicked.connect(self.pausescrapping)
        
    def update_progress(self, n):
            self.progress.setValue(n)
        
        
        #tn_pause.pressed.connect(self.runner.pause)
        #btn_resume.pressed.connect(self.runner.resume)
        #btn_done.pressed.connect(self.runner.pause)
        
    '''
     #---Function to start scrapping---     
    def Start_Scrapping(self):
        try:
            url='https://www.imdb.com/search/title/?title_type=feature&release_date=2015-01-01,2015-12-31&after=WzUxOTE1LCJ0dDkxNzYyNjAiLDEwMTAxXQ%3D%3D&ref_=adv_nxt'
            
            Rank=[]
            Rank2=[]
            Mname=[]
            Year=[]
            Genre=[]
            Restrict=[]
            Rating=[]
            Metascore=[]
            Director=[]
            
            while True:
                # print(url)
                source=requests.get(url)
                soup=BeautifulSoup(source.text,'html.parser')
                nextpage=soup.select_one('div.desc>a')
                if nextpage:
                    nexturl=nextpage.get('href')
                    url=urljoin(url, nexturl)
                else:
                    break 
            
                source=requests.get(url)
                source.raise_for_status()

                soup = BeautifulSoup(source.text,'html.parser')
            
                movies = soup.find('div' , class_ = "lister-list").find_all('div', class_ = "lister-item mode-advanced")
                
                for dt in movies:
                    
                    if dt.find('div',class_="lister-item-content"):    
                        rank=dt.find('div',class_="lister-item-content").span.text
                        mname=dt.find('div',class_="lister-item-content").a.text
                        year=dt.find('div',class_="lister-item-content").find('span',class_="lister-item-year text-muted unbold").text
                    else:
                        rank,Mname,year="NA"
                        
                    if dt.find('p',class_="text-muted"):
                        if dt.find('span',class_="genre"):
                            genre=dt.find('span',class_="genre").text
                    else:
                        genre="NA"

                    if dt.find('span', class_ = "certificate"):
                        restrict = dt.find('span', class_ = "certificate").text
                    else:
                        restrict = 'NA'

                    if dt.find('div',class_="inline-block ratings-imdb-rating"):
                        rating=dt.find('strong').text
                    else:
                        rating="0.0"

                    if(dt.find('div', class_ = "inline-block ratings-metascore")):
                        metascore = dt.find('div', class_ = "inline-block ratings-metascore").span.text
                    else:
                        metascore = 0
                    
                                
                    if(dt.find('p', class_= "").find('a')):
                        director = dt.find('p',class_="").a.text
                    else:
                        director = 'NA'
               
                        
                    print(rank,Mname,year,genre,restrict,rating,metascore,director)
                    
                    
                    Rank.append(rank.replace(',', ''))
                    Mname.append(mname)
                    year = year.strip('(IV)')
                    year = year.strip('(I)')
                    year = year.strip('(')
                    year = year.strip(' (')
                    year = year.strip(')')
                    year = year.strip('-')
                    Year.append(year)
                    Genre.append(genre.strip())
                    Restrict.append(restrict)
                    Rating.append(rating)
                    Metascore.append(metascore)
                    Director.append(director)
                    
                df = pd.DataFrame({'Rank':Rank,'Movies Name':Mname,'Year':Year,'Genre':Genre,'PG-Rating':Restrict,'Rating':Rating,'MetaScore':Metascore,'Director':Director}) 
                df.to_csv('products.csv', index=False, encoding='utf-8')
                
                    

        except Exception as e:
            print(e)
            
    def pausescrapping(self):
            while True:
               #try:
                    time.sleep(1)  # do something here
                    self.Start_Scrapping()

                #except KeyboardInterrupt:
                    if(self.pause.clicked):
                       print ('\nPausing...  (Hit ENTER to continue, type quit to exit.)')
                       try:
                           response = input()
                           if response == 'quit':
                               break
                           print ('Resuming...')
                       except KeyboardInterrupt:
                           print ('Resuming...')
                           continue'''
    #---After scrapping go to next page code---
    def gotoNextScreen(self):
        mainwindow = MainWindow()
        widget.addWidget(mainwindow)
        widget.setCurrentIndex(widget.currentIndex()+1)

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput)) 
data.remove(data[0])       
rank,name,year,genre,pg,rat,meta,direc = [],[],[],[],[],[],[],[]
'''
for i in data:
    i[2] = i[2].strip('(IV)')
    i[2] = i[2].strip('(I)')
    i[2] = i[2].strip('(')
    i[2] = i[2].strip(' (')
    i[2] = i[2].strip(')')
    i[2] = i[2].strip('-')'''
for i in data:
    rank.append(i[0])
    name.append(i[1])
    year.append(int(i[2]))
    genre.append(i[3])
    pg.append(i[4])
    rat.append(float(i[5]))
    meta.append(int(i[6]))
    direc.append(i[7])

#---Class of page containing the data grid---
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        loadUi("MoviesProject2.ui",self)
        self.MoviesTable.setColumnWidth(0, 250)
        self.MoviesTable.setColumnWidth(1, 100)
        self.MoviesTable.setColumnWidth(2, 250)
        self.loaddata(data)
        self.SearchBar.textChanged.connect(self.SearchItem)
        self.Apply.clicked.connect(self.ApplySortingAlgo)
        self.MoviesTable.clicked.connect(self.fill_values)
        self.DeleteButton.clicked.connect(self.Delete_Movie)
        
    #---Function to load data from the file---   
    def loaddata(self, data):
        tablerow=0
        self.MoviesTable.setRowCount(len(data))
        #print(data)
        for row in data:
            self.MoviesTable.setItem(tablerow, 0, QtWidgets.QTableWidgetItem(row[0]))
            self.MoviesTable.setItem(tablerow, 1, QtWidgets.QTableWidgetItem(row[1]))
            self.MoviesTable.setItem(tablerow, 2, QtWidgets.QTableWidgetItem(row[2]))
            self.MoviesTable.setItem(tablerow, 3, QtWidgets.QTableWidgetItem(row[3]))
            self.MoviesTable.setItem(tablerow, 4, QtWidgets.QTableWidgetItem(row[4]))
            self.MoviesTable.setItem(tablerow, 5, QtWidgets.QTableWidgetItem(row[5]))
            self.MoviesTable.setItem(tablerow, 6, QtWidgets.QTableWidgetItem(row[6]))
            self.MoviesTable.setItem(tablerow, 7, QtWidgets.QTableWidgetItem(row[7]))

            tablerow+=1

     #---Searching Algorithms---#       
    def SearchItem(self):
        start = time.time()
        if(self.SearchAlgo.currentText()=='Linear'):
            self.LinearSearch()
               
        if(self.SearchAlgo.currentText()=='Binary'):
            
            count = 0
            arr = []
            index = 0
            table_rows = self.MoviesTable.rowCount()
            for i in range(self.MoviesTable.rowCount()):
                arr.append(float(self.MoviesTable.item(i, 5).text()))  
            x = self.SearchBar.text()
            n = len(arr)
            #print(arr)
            if table_rows != 0 and len(x) != 0: 
                
             if(len(x)>=2):
                x = float(x)
                index = self.binarySearch(arr, x, 0, n-1)
             for i in range(self.MoviesTable.rowCount()):
                if(x==float(self.MoviesTable.item(i, 5).text())):
                    count = count+1
             
             for i in range(0, index):
                 self.MoviesTable.hideRow(i)
             for i in range(index+count+1, self.MoviesTable.rowCount()):
                 self.MoviesTable.hideRow(i)
             for i in range(index, index+count):
                 self.MoviesTable.showRow(i)
            else:
                for i in range(self.MoviesTable.rowCount()):
                   self.MoviesTable.showRow(i)
        end = time.time()
        total = str(end-start)
        self.TimeTaken.setText(total+' s')
        
        #---Binary Search---Only for IMDB-Rating---#
    def binarySearch(self,array, x, low, high):
        
        # Repeat until the pointers low and high meet each other
        flag = False
        while low <= high:   
          mid = low + (high - low)//2
          if flag == True:
              break
          if array[mid] == x:
              while (array[mid]== x):
                  mid = mid-1
                  print(mid)
              return mid+1
              flag = True
              break
          elif array[mid] < x:
              low = mid + 1

          else: 
             high = mid - 1

        return -1

    #---Linear Search---#
    def LinearSearch(self):
        name = self.SearchBar.text()
        table_rows = self.MoviesTable.rowCount()
        if table_rows != 0 and len(name) != 0:
            for ro in range(self.MoviesTable.rowCount()):
                if name not in self.MoviesTable.item(ro, 0).text():
                    if name not in self.MoviesTable.item(ro, 1).text():
                        if name not in self.MoviesTable.item(ro, 2).text():
                            if name not in self.MoviesTable.item(ro, 3).text():
                                if name not in self.MoviesTable.item(ro, 4).text():
                                    if name not in self.MoviesTable.item(ro, 5).text():
                                        if name not in self.MoviesTable.item(ro, 6).text():
                                            self.MoviesTable.hideRow(ro)
        else:
            for ro in range(table_rows):
                self.MoviesTable.showRow(ro)

#---Sorting Algorithms---#
    def ApplySortingAlgo(self):
        start = time.time()
        size1 = len(data)
        att = self.SortBy.currentText()
        AD = self.AoD.currentText()
#---Insertion Condition---#
        if(self.Sort.currentText() == 'Insertion sort'):
            self.InsertionSort(att)
#---Radix Condition---#
        elif(self.Sort.currentText() == 'Radix sort' and att=='Year'):
            self.radixSortOnYear(year)
        elif(self.Sort.currentText() == 'Radix sort' and att=='IMDB-Rating'):
            self.radixSortOnIMBD(rat)
        elif(self.Sort.currentText() == 'Radix sort' and att=='Metascore'):
            self.radixSortOnMetaScore(meta)
#---Quick Condition---#
        elif(self.Sort.currentText() == 'Quick sort' and att=='Director' and AD == 'Ascending'):
            self.quickSortWithDirectionA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Director' and AD == 'Descending'):
            self.quickSortWithDirectionD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Genre' and AD == 'Ascending'):
            self.quickSortWithGenreA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Genre' and AD == 'Descending'):
            self.quickSortWithGenreD(data, 0, size1-1) 
        elif(self.Sort.currentText() == 'Quick sort' and att=='IMDB-Rating' and AD == 'Ascending'):
            self.quickSortWithIMBDA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='IMDB-Rating' and AD == 'Descending'):
            self.quickSortWithIMBDD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Metascore' and AD == 'Ascending'):
            self.quickSortWithMetaA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Metascore' and AD == 'Descending'):
            self.quickSortWithMetaD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Movie name' and AD == 'Ascending'):
            self.quickSortWithNameA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Movie name' and AD == 'Descending'):
            self.quickSortWithNameD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='PG-Rating' and AD == 'Ascending'):
            self.quickSortWithPgA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='PG-Rating' and AD == 'Descending'):
            self.quickSortWithPgD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Rank' and AD == 'Ascending'):
            self.quickSortWithRankA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Rank' and AD == 'Descending'):
            self.quickSortWithRankD(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Year' and AD == 'Ascending'):
            self.quickSortWithYearA(data, 0, size1-1)
        elif(self.Sort.currentText() == 'Quick sort' and att=='Year' and AD == 'Descending'):
            self.quickSortWithYearD(data, 0, size1-1)
#---Bubble Sort---#
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Director' and AD == 'Ascending'):
            self.bubbleSortWithDirectionA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Director' and AD == 'Descending'):
            self.bubbleSortWithDirectionD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Genre' and AD == 'Ascending'):
            self.bubbleSortWithGenreA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Genre' and AD == 'Descending'):
            self.bubbleSortWithGenreD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='IMDB-Rating' and AD == 'Ascending'):
            self.bubbleSortWithIMBDA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='IMDB-Rating' and AD == 'Descending'):
            self.bubbleSortWithIMBDD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Metascore' and AD == 'Ascending'):
            self.bubbleSortWithMetaA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Metascore' and AD == 'Descending'):
            self.bubbleSortWithMetaD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Movie name' and AD == 'Ascending'):
            self.bubbleSortWithMovieA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Movie name' and AD == 'Descending'):
            self.bubbleSortWithMovieD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='PG-Rating' and AD == 'Ascending'):
            self.bubbleSortWithPGA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='PG-Rating' and AD == 'Descending'):
            self.bubbleSortWithPGD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Rank' and AD == 'Ascending'):
            self.bubbleSortWithRankA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Rank' and AD == 'Descending'):
            self.bubbleSortWithRankD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Rank' and AD == 'Ascending'):
            self.bubbleSortWithRankA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Rank' and AD == 'Descending'):
            self.bubbleSortWithRankD(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Year' and AD == 'Ascending'):
            self.bubbleSortWithYearA(data)
        elif(self.Sort.currentText() == 'Bubble sort' and att=='Year' and AD == 'Descending'):
            self.bubbleSortWithYearD(data)
#---Selection Condition---#
        elif(self.Sort.currentText() == 'Selection sort'):
             self.selectionSort(data, size1, att, AD)
#---Counting Condition---#
        elif(self.Sort.currentText() == 'counting sort' and att=='IMDB-Rating'):
            self.countingSortOnRat(rat)
#---Heap Sort---#
        elif(self.Sort.currentText() == 'Heap sort' and att=='Rank' and AD == 'Ascending'):
            self.heapSortRA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Rank' and AD == 'Descending'):
            self.heapSortRD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Movie name' and AD == 'Ascending'):
            self.heapSortMA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Movie name' and AD == 'Descending'):
            self.heapSortMD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Year' and AD == 'Ascending'):
            self.heapSortYA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Year' and AD == 'Descending'):
            self.heapSortYD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Genre' and AD == 'Ascending'):
            self.heapSortGA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Genre' and AD == 'Descending'):
            self.heapSortGD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='PG-Rating' and AD == 'Ascending'):
            self.heapSortPA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='PG-Rating' and AD == 'Descending'):
            self.heapSortPD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='IMDB-Rating' and AD == 'Ascending'):
            self.heapSortIA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='IMDB-Rating' and AD == 'Descending'):
            self.heapSortID(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Metascore' and AD == 'Ascending'):
            self.heapSortMtA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Metascore' and AD == 'Descending'):
            self.heapSortMtD(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Director' and AD == 'Ascending'):
            self.heapSortDA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Director' and AD == 'Descending'):
            self.heapSortDD(data)
#---Bucket Condition---#
        elif(self.Sort.currentText() == 'Bucket sort' and att=='IMDB-Rating' and AD == 'Ascending'):
            self. bucketSortWithIMBDA(data)
        elif(self.Sort.currentText() == 'Heap sort' and att=='Rank' and AD == 'Ascending'):
            self.bucketSortWithRankA(data)
        
        end = time.time()
        total = str(end-start)
        self.TimeTaken.setText(total+' s')
        
    #---1.---Insertion Sort---#
    def InsertionSort(self, Attribute):
        if(Attribute=='Rank'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][0]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][0] > c1):
                    data[j+1][0] = data[j][0]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][0] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)
        elif(Attribute=='Movie name'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][1]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][1] > c1):
                    data[j+1][1] = data[j][1]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][1] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)
        elif(Attribute=='Year'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][2]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][2] > c1):
                    data[j+1][2] = data[j][2]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][2] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)
        elif(Attribute=='Genre'):
             self.MoviesTable.setRowCount(len(data))
             for i in range(len(data)):
                 c1 = data[i][3]
                 c2 = data[i][0]
                 c3 = data[i][1]
                 c4 = data[i][2]
                 c5 = data[i][4]
                 j = i-1
                 while(j >= 0 and data[j][3] > c1):
                     data[j+1][3] = data[j][3]
                     data[j+1][0] = data[j][0]
                     data[j+1][1] = data[j][1]
                     data[j+1][2] = data[j][2]
                     data[j+1][4] = data[j][4]
                     j = j-1
                 data[j + 1][3] = c1
                 data[j + 1][0] = c2
                 data[j + 1][1] = c3
                 data[j + 1][2] = c4
                 data[j + 1][4] = c5
             
             #This is to reload the table so that recent added data shows in the table
             self.loaddata(data)
        elif(Attribute=='IMDB-Rating'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][5]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][5] > c1):
                    data[j+1][5] = data[j][5]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][5] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)
        elif(Attribute=='Metascore'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][6]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][6] > c1):
                    data[j+1][6] = data[j][6]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][6] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)
        elif(Attribute=='Director'):
            self.MoviesTable.setRowCount(len(data))
            for i in range(len(data)):
                c1 = data[i][7]
                c2 = data[i][0]
                c3 = data[i][1]
                c4 = data[i][2]
                c5 = data[i][4]
                j = i-1
                while(j >= 0 and data[j][7] > c1):
                    data[j+1][7] = data[j][7]
                    data[j+1][0] = data[j][0]
                    data[j+1][1] = data[j][1]
                    data[j+1][2] = data[j][2]
                    data[j+1][4] = data[j][4]
                    j = j-1
                data[j + 1][7] = c1
                data[j + 1][0] = c2
                data[j + 1][1] = c3
                data[j + 1][2] = c4
                data[j + 1][4] = c5
            #This is to reload the table so that recent added data shows in the table
            self.loaddata(data)

    #---4.---Radix Sort---#
    # Using counting sort to sort the elements in the basis of significant places
    def countingSort(self,rat1, place):
        size = len(rat1)
        o0 = [0]*size
        o1 = [0]*size
        o2 = [0]*size
        o3 = [0]*size
        o4 = [0]*size
        o5 = [0]*size
        o6 = [0]*size
        o7 = [0]*size
        count = [0] * 10

        # Calculate count of elements
        for i in range(0, size):
            index = int(rat1[i] // place)
            count[index % 10] += 1

        # Calculate cumulative count
        for i in range(1, 10):
            count[i] += count[i - 1]

        # Place the elements in sorted order
        i = size - 1
        while i >= 0:
            index = int(rat1[i] // place)
            o0[count[index % 10] - 1] = rank[i]
            o1[count[index % 10] - 1] = name[i]
            o2[count[index % 10] - 1] = year[i]
            o3[count[index % 10] - 1] = genre[i]
            o4[count[index % 10] - 1] = pg[i]
            o5[count[index % 10] - 1] = rat[i]
            o6[count[index % 10] - 1] = meta[i]
            o7[count[index % 10] - 1] = direc[i]
            count[index % 10] -= 1
            i -= 1

        for i in range(0, size):
            rank[i] = o0[i]
            name[i] = o1[i]
            year[i] = o2[i]
            genre[i] = o3[i]
            pg[i] = o4[i]
            rat[i] = o5[i]
            meta[i] = o6[i]
            direc[i] = o7[i]
        
    def radixSortOnYear(self,val1):
        # Get maximum element
        max_element = max(val1)

        # Apply counting sort to sort elements based on place value.
        place = 1
        while max_element // place > 0:
            self.countingSort(val1, place)
            place *= 10
        val = []
        for i in range(len(rank)):
            newD = []
            newD.append(rank[i])
            newD.append(name[i])
            newD.append(str(year[i]))
            newD.append(genre[i])
            newD.append(pg[i])
            newD.append(str(rat[i]))
            newD.append(str(meta[i]))
            newD.append(direc[i])
            val.append(newD)
        data = val
        print(data)
        
    def radixSortOnIMBD(self,val1):
        # Get maximum element
        max_element = max(val1)

        # Apply counting sort to sort elements based on place value.
        place = 1
        while max_element // place > 0:
            self.countingSort(val1, place)
            place *= 10
        val = []
        for i in range(len(rank)):
            newD = []
            newD.append(rank[i])
            newD.append(name[i])
            newD.append(str(year[i]))
            newD.append(genre[i])
            newD.append(pg[i])
            newD.append(str(rat[i]))
            newD.append(str(meta[i]))
            newD.append(direc[i])
            val.append(newD)
        data = val
        self.loaddata(data)
        
    def radixSortOnMetaScore(self,val1):
        # Get maximum element
        max_element = max(val1)

        # Apply counting sort to sort elements based on place value.
        place = 1
        while max_element // place > 0:
            self.countingSort(val1, place)
            place *= 10
        val = []
        for i in range(len(rank)):
            newD = []
            newD.append(rank[i])
            newD.append(name[i])
            newD.append(str(year[i]))
            newD.append(genre[i])
            newD.append(pg[i])
            newD.append(str(rat[i]))
            newD.append(str(meta[i]))
            newD.append(direc[i])
            val.append(newD)
        data = val
        self.loaddata(data)
        
    #---9.---Quick Sort---#    
    # function to find the partition position
    def partitionDA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][7]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][7] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithDirectionA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionDA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithDirectionA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithDirectionA(array, pi + 1, high)

        self.loaddata(array)
        
    def partitionDD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][7]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][7] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithDirectionD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionDD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithDirectionD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithDirectionD(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionGA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][3]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][3] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithGenreA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionGA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithGenreA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithGenreA(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionGD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][3]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][3] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithGenreD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionGD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithGenreD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithGenreD(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionIA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][5]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][5] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithIMBDA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionIA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithIMBDA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithIMBDA(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionID(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][5]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][5] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithIMBDD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionID(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithIMBDD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithIMBDD(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionMA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][6]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][6] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithMetaA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionMA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithMetaA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithMetaA(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionMD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][6]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][6] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithMetaD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionMD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithMetaD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithMetaD(array, pi + 1, high)
        
        self.loaddata(array)
        
    def partitionNA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][1]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][1] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithNameA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionNA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithNameA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithNameA(array, pi + 1, high)
        
        self.loaddata(array)
 
    # function to find the partition position

    def partitionPA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][4]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][4] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithPgA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionPA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithPgA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithPgA(array, pi + 1, high)
        
        self.loaddata(array)

    def partitionND(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][1]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][1] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithNameD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionND(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithNameD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithNameD(array, pi + 1, high)
        
        self.loaddata(array)

    def partitionPD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][4]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][4] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithPgD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionPD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithPgD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithPgD(array, pi + 1, high)
        
        self.loaddata(array)

    def partitionRA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][0]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][0] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithRankA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionRA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithRankA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithRankA(array, pi + 1, high)
        
        self.loaddata(array)

    def partitionRD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][5]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][5] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithRankD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionRD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithRankD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithRankD(array, pi + 1, high)
        
        self.loaddata(array)

    # function to find the partition position
    def partitionYA(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][2]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][2] <= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithYearA(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionYA(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithYearA(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithYearA(array, pi + 1, high)
        
    def partitionYD(self,array, low, high):

      # choose the rightmost element as pivot
      pivot = array[high][2]

      # pointer for greater element
      i = low - 1

      # traverse through all elements
      # compare each element with pivot
      for j in range(low, high):
        if array[j][2] >= pivot:
          # if element smaller than pivot is found
          # swap it with the greater element pointed by i
          i = i + 1

          # swapping element at i with element at j
          (array[i], array[j]) = (array[j], array[i])

      # swap the pivot element with the greater element specified by i
      (array[i + 1], array[high]) = (array[high], array[i + 1])

      # return the position from where partition is done
      return i + 1

    # function to perform quicksort
    def quickSortWithYearD(self,array, low, high):
      if low < high:

        # find pivot element such that
        # element smaller than pivot are on the left
        # element greater than pivot are on the right
        pi = self.partitionYD(array, low, high)

        # recursive call on the left of pivot
        self.quickSortWithYearD(array, low, pi - 1)

        # recursive call on the right of pivot
        self.quickSortWithYearD(array, pi + 1, high)
        
        self.loaddata(array)
        
#---Bubble Sort---#
    def bubbleSortWithDirectionA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][7] > data[j + 1][7]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
                    
        self.loaddata(data)
                    
    def bubbleSortWithDirectionD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][7] < data[j + 1][7]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
        
    def bubbleSortWithGenreA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][3] > data[j + 1][3]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
        
    def bubbleSortWithGenreD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][3] < data[j + 1][3]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithIMBDA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][5] > data[j + 1][5]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithIMBDD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][5] < data[j + 1][5]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithMetaA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][6] > data[j + 1][6]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithMetaD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][6] < data[j + 1][6]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithMovieA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][1] > data[j + 1][1]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithMovieD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][1] < data[j + 1][1]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithPGA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][4] > data[j + 1][4]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithPGD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if data[j][4] < data[j + 1][4]:

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithRankA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if float(data[j][0]) > float(data[j + 1][0]):

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithRankD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if float(data[j][0]) < float(data[j + 1][0]):

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithYearA(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if float(data[j][2]) > float(data[j + 1][2]):

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
    def bubbleSortWithYearD(self,data):
        length = len(data)
        # loop to access each data element
        for i in range(length):

            # loop to compare data elements
            for j in range(0, length - i - 1):
                  
                # compare two adjacent elements
                # change > to < to sort in descending order
                if float(data[j][2]) < float(data[j + 1][2]):

                    # swapping elements if elements
                    # are not in the intended order
                    temp = data[j]
                    data[j] = data[j+1]
                    data[j+1] = temp
        self.loaddata(data)
        
#---Selection Sort---#
    def selectionSort(self,array, size, attr, ad):
        
        if(attr=='Director' and ad=='Ascending'):
            for step in range(size):
               min_idx = step

               for i in range(step + 1, size):
                
                   # to sort in descending order, change > to < in this line
                   # select the minimum element in each loop
                   if array[i][7] < array[min_idx][7]:
                       min_idx = i
                
               # put min at the correct position
               (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array) 
        
        elif(attr=='Director' and ad=='Descending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][7] > array[min_idx][7]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
            
        elif(attr=='Genre' and ad=='Ascending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][3] < array[min_idx][3]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
            
        elif(attr=='Genre' and ad=='Descending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][3] > array[min_idx][3]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
            
        elif(attr=='IMDB-Rating' and ad=='Ascending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][5] < array[min_idx][5]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
        
        elif(attr=='IMDB-Rating' and ad=='Descending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][5] > array[min_idx][5]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
    
        elif(attr=='Metascore' and ad=='Ascending'):
            for step in range(size):
                min_idx = step

                for i in range(step + 1, size):
                 
                    # to sort in descending order, change > to < in this line
                    # select the minimum element in each loop
                    if array[i][6] < array[min_idx][6]:
                        min_idx = i
                 
                # put min at the correct position
                (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
        elif(attr=='Metascore' and ad=='Descending'):
            for step in range(size):
                 min_idx = step

                 for i in range(step + 1, size):
                  
                     # to sort in descending order, change > to < in this line
                     # select the minimum element in each loop
                     if array[i][6] > array[min_idx][6]:
                         min_idx = i
                  
                 # put min at the correct position
                 (array[step], array[min_idx]) = (array[min_idx], array[step])
            self.loaddata(array)
            
        elif(attr=='Movie name' and ad=='Ascending'):
             for step in range(size):
                 min_idx = step

                 for i in range(step + 1, size):
                  
                     # to sort in descending order, change > to < in this line
                     # select the minimum element in each loop
                     if array[i][1] < array[min_idx][1]:
                         min_idx = i
                  
                 # put min at the correct position
                 (array[step], array[min_idx]) = (array[min_idx], array[step])
             self.loaddata(array)
             
        elif(attr=='Movie name' and ad=='Descending'):
              for step in range(size):
                  min_idx = step

                  for i in range(step + 1, size):
                   
                      # to sort in descending order, change > to < in this line
                      # select the minimum element in each loop
                      if array[i][1] > array[min_idx][1]:
                          min_idx = i
                   
                  # put min at the correct position
                  (array[step], array[min_idx]) = (array[min_idx], array[step])
              self.loaddata(array)
        
        elif(attr=='PG-Rating' and ad=='Ascending'):
                for step in range(size):
                    min_idx = step

                    for i in range(step + 1, size):
                     
                        # to sort in descending order, change > to < in this line
                        # select the minimum element in each loop
                        if array[i][4] < array[min_idx][4]:
                            min_idx = i
                     
                    # put min at the correct position
                    (array[step], array[min_idx]) = (array[min_idx], array[step])
                self.loaddata(array)
                
        elif(attr=='PG-Rating' and ad=='Descending'):
                 for step in range(size):
                     min_idx = step

                     for i in range(step + 1, size):
                      
                         # to sort in descending order, change > to < in this line
                         # select the minimum element in each loop
                         if array[i][4] > array[min_idx][4]:
                             min_idx = i
                      
                     # put min at the correct position
                     (array[step], array[min_idx]) = (array[min_idx], array[step])
                 self.loaddata(array)
                 
        elif(attr=='Rank' and ad=='Ascending'):
                  for step in range(size):
                      min_idx = step

                      for i in range(step + 1, size):
                       
                          # to sort in descending order, change > to < in this line
                          # select the minimum element in each loop
                          if array[i][0] < array[min_idx][0]:
                              min_idx = i
                       
                      # put min at the correct position
                      (array[step], array[min_idx]) = (array[min_idx], array[step])
                  self.loaddata(array)
                  
        elif(attr=='Rank' and ad=='Descending'):
                  for step in range(size):
                      min_idx = step

                      for i in range(step + 1, size):
                       
                          # to sort in descending order, change > to < in this line
                          # select the minimum element in each loop
                          if array[i][0] > array[min_idx][0]:
                              min_idx = i
                       
                      # put min at the correct position
                      (array[step], array[min_idx]) = (array[min_idx], array[step])
                  self.loaddata(array)
                  
        elif(attr=='Year' and ad=='Ascending'):
                  for step in range(size):
                      min_idx = step

                      for i in range(step + 1, size):
                       
                          # to sort in descending order, change > to < in this line
                          # select the minimum element in each loop
                          if array[i][2] < array[min_idx][2]:
                              min_idx = i
                       
                      # put min at the correct position
                      (array[step], array[min_idx]) = (array[min_idx], array[step])
                  self.loaddata(array)
                  
        elif(attr=='Year' and ad=='Descending'):
                  for step in range(size):
                      min_idx = step

                      for i in range(step + 1, size):
                       
                          # to sort in descending order, change > to < in this line
                          # select the minimum element in each loop
                          if array[i][2] > array[min_idx][2]:
                              min_idx = i
                       
                      # put min at the correct position
                      (array[step], array[min_idx]) = (array[min_idx], array[step])
                  self.loaddata(array)
#---Counting Sort---#                  
    def countingSortOnRat(self,rat1):
        
        size = len(rat1)
        o0 = [0] * size
        o1,o2,o3,o4,o5,o6,o7=[0]* size,[0]* size,[0]* size,[0]* size,[0]* size,[0]* size,[0]* size
        # Initialize count rat
        count = [0] * 10

        # Store the count of each elements in count rat
        for i in range(0, size):
            count[int(rat1[i])] += 1

        # Store the cummulative count
        for i in range(1, 10):
            count[i] += count[i - 1]

        # Find the index of each element of the original rat in count rat
        # place the elements in output rat
        i = size - 1
        while i >= 0:
            o0[count[int(rat1[i])] - 1] = rank[i]
            o1[count[int(rat1[i])] - 1] = name[i]
            o2[count[int(rat1[i])] - 1] = year[i]
            o3[count[int(rat1[i])] - 1] = genre[i]
            o4[count[int(rat1[i])] - 1] = pg[i]
            o5[count[int(rat1[i])] - 1] = rat[i]
            o6[count[int(rat1[i])] - 1] = meta[i]
            o7[count[int(rat1[i])] - 1] = direc[i]
            count[int(rat1[i])] -= 1
            i -= 1

        # Copy the sorted elements into original rat
        for i in range(0, size):
            rank[i] = o0[i]
            name[i] = o1[i]
            year[i] = o2[i]
            genre[i] = o3[i]
            pg[i] = o4[i]
            rat[i] = o5[i]
            meta[i] = o6[i]
            direc[i] = o7[i]
            
        val = []
        for i in range(len(rank)):
            newD = []
            newD.append(rank[i])
            newD.append(name[i])
            newD.append(str(year[i]))
            newD.append(genre[i])
            newD.append(pg[i])
            newD.append(str(rat[i]))
            newD.append(str(meta[i]))
            newD.append(direc[i])
            val.append(newD)
        data = val
        self.loaddata(data)
#---Heap Sort---#
    def heapifyRA(self,arr, n, i):
          # Find largest among root and children
          largest = i
          l = 2 * i + 1
          r = 2 * i + 2
      
          if l < n and arr[i][0] < arr[l][0]:
              largest = l
      
          if r < n and arr[largest][0] < arr[r][0]:
              largest = r
      
          # If root is not largest, swap with largest and continue heapifying
          if largest != i:
              arr[i], arr[largest] = arr[largest], arr[i]
              self. heapifyRA(arr, n, largest)
      
      
    def heapSortRA(self,arr):
          n = len(arr)
      
          # Build max heap
          for i in range(n//2, -1, -1):
              self.heapifyRA(arr, n, i)
      
          for i in range(n-1, 0, -1):
              # Swap
              arr[i], arr[0] = arr[0], arr[i]
      
              # Heapify root element
              self.heapifyRA(arr, i, 0)
          self.loaddata(arr)
          
    def heapifyRD(self,arr, n, i):
           # Find largest among root and children
           largest = i
           l = 2 * i + 1
           r = 2 * i + 2
       
           if l < n and arr[i][0] > arr[l][0]:
               largest = l
       
           if r < n and arr[largest][0] > arr[r][0]:
               largest = r
       
           # If root is not largest, swap with largest and continue heapifying
           if largest != i:
               arr[i], arr[largest] = arr[largest], arr[i]
               self. heapifyRD(arr, n, largest)
       
       
    def heapSortRD(self,arr):
           n = len(arr)
       
           # Build max heap
           for i in range(n//2, -1, -1):
               self.heapifyRD(arr, n, i)
       
           for i in range(n-1, 0, -1):
               # Swap
               arr[i], arr[0] = arr[0], arr[i]
       
               # Heapify root element
               self.heapifyRD(arr, i, 0)
           self.loaddata(arr)
           
    def heapifyMA(self,arr, n, i):
          # Find largest among root and children
          largest = i
          l = 2 * i + 1
          r = 2 * i + 2
      
          if l < n and arr[i][1] < arr[l][1]:
              largest = l
      
          if r < n and arr[largest][1] < arr[r][1]:
              largest = r
      
          # If root is not largest, swap with largest and continue heapifying
          if largest != i:
              arr[i], arr[largest] = arr[largest], arr[i]
              self. heapifyMA(arr, n, largest)
              
      
      
    def heapSortMA(self,arr):
          n = len(arr)
      
          # Build max heap
          for i in range(n//2, -1, -1):
              self.heapifyMA(arr, n, i)
      
          for i in range(n-1, 0, -1):
              # Swap
              arr[i], arr[0] = arr[0], arr[i]
      
              # Heapify root element
              self.heapifyMA(arr, i, 0)
          self.loaddata(arr)
          
    def heapifyMD(self,arr, n, i):
           # Find largest among root and children
           largest = i
           l = 2 * i + 1
           r = 2 * i + 2
       
           if l < n and arr[i][1] > arr[l][1]:
               largest = l
       
           if r < n and arr[largest][1] > arr[r][1]:
               largest = r
       
           # If root is not largest, swap with largest and continue heapifying
           if largest != i:
               arr[i], arr[largest] = arr[largest], arr[i]
               self. heapifyMD(arr, n, largest)
       
       
    def heapSortMD(self,arr):
           n = len(arr)
       
           # Build max heap
           for i in range(n//2, -1, -1):
               self.heapifyMD(arr, n, i)
       
           for i in range(n-1, 0, -1):
               # Swap
               arr[i], arr[0] = arr[0], arr[i]
       
               # Heapify root element
               self.heapifyMD(arr, i, 0)
           self.loaddata(arr)
           
    def heapifyYA(self,arr, n, i):
          # Find largest among root and children
          largest = i
          l = 2 * i + 1
          r = 2 * i + 2
      
          if l < n and arr[i][2] < arr[l][2]:
              largest = l
      
          if r < n and arr[largest][2] < arr[r][2]:
              largest = r
      
          # If root is not largest, swap with largest and continue heapifying
          if largest != i:
              arr[i], arr[largest] = arr[largest], arr[i]
              self. heapifyYA(arr, n, largest)
              
      
      
    def heapSortYA(self,arr):
          n = len(arr)
      
          # Build max heap
          for i in range(n//2, -1, -1):
              self.heapifyYA(arr, n, i)
      
          for i in range(n-1, 0, -1):
              # Swap
              arr[i], arr[0] = arr[0], arr[i]
      
              # Heapify root element
              self.heapifyYA(arr, i, 0)
          self.loaddata(arr)
          
    def heapifyYD(self,arr, n, i):
           # Find largest among root and children
           largest = i
           l = 2 * i + 1
           r = 2 * i + 2
       
           if l < n and arr[i][2] > arr[l][2]:
               largest = l
       
           if r < n and arr[largest][2] > arr[r][2]:
               largest = r
       
           # If root is not largest, swap with largest and continue heapifying
           if largest != i:
               arr[i], arr[largest] = arr[largest], arr[i]
               self. heapifyYD(arr, n, largest)
       
       
    def heapSortYD(self,arr):
           n = len(arr)
       
           # Build max heap
           for i in range(n//2, -1, -1):
               self.heapifyYD(arr, n, i)
       
           for i in range(n-1, 0, -1):
               # Swap
               arr[i], arr[0] = arr[0], arr[i]
       
               # Heapify root element
               self.heapifyYD(arr, i, 0)
           self.loaddata(arr)     
           
    def heapifyGA(self,arr, n, i):
          # Find largest among root and children
          largest = i
          l = 2 * i + 1
          r = 2 * i + 2
      
          if l < n and arr[i][3] < arr[l][3]:
              largest = l
      
          if r < n and arr[largest][3] < arr[r][3]:
              largest = r
      
          # If root is not largest, swap with largest and continue heapifying
          if largest != i:
              arr[i], arr[largest] = arr[largest], arr[i]
              self. heapifyGA(arr, n, largest)
              
      
      
    def heapSortGA(self,arr):
          n = len(arr)
      
          # Build max heap
          for i in range(n//2, -1, -1):
              self.heapifyGA(arr, n, i)
      
          for i in range(n-1, 0, -1):
              # Swap
              arr[i], arr[0] = arr[0], arr[i]
      
              # Heapify root element
              self.heapifyGA(arr, i, 0)
          self.loaddata(arr)
          
    def heapifyGD(self,arr, n, i):
           # Find largest among root and children
           largest = i
           l = 2 * i + 1
           r = 2 * i + 2
       
           if l < n and arr[i][3] > arr[l][3]:
               largest = l
       
           if r < n and arr[largest][3] > arr[r][3]:
               largest = r
       
           # If root is not largest, swap with largest and continue heapifying
           if largest != i:
               arr[i], arr[largest] = arr[largest], arr[i]
               self. heapifyGD(arr, n, largest)
       
       
    def heapSortGD(self,arr):
           n = len(arr)
       
           # Build max heap
           for i in range(n//2, -1, -1):
               self.heapifyGD(arr, n, i)
       
           for i in range(n-1, 0, -1):
               # Swap
               arr[i], arr[0] = arr[0], arr[i]
       
               # Heapify root element
               self.heapifyGD(arr, i, 0)
           self.loaddata(arr)
           
    def heapifyPA(self,arr, n, i):
          # Find largest among root and children
          largest = i
          l = 2 * i + 1
          r = 2 * i + 2
      
          if l < n and arr[i][4] < arr[l][4]:
              largest = l
      
          if r < n and arr[largest][4] < arr[r][4]:
              largest = r
      
          # If root is not largest, swap with largest and continue heapifying
          if largest != i:
              arr[i], arr[largest] = arr[largest], arr[i]
              self. heapifyPA(arr, n, largest)
              
      
      
    def heapSortPA(self,arr):
          n = len(arr)
      
          # Build max heap
          for i in range(n//2, -1, -1):
              self.heapifyPA(arr, n, i)
      
          for i in range(n-1, 0, -1):
              # Swap
              arr[i], arr[0] = arr[0], arr[i]
      
              # Heapify root element
              self.heapifyPA(arr, i, 0)
          self.loaddata(arr)
          
    def heapifyPD(self,arr, n, i):
           # Find largest among root and children
           largest = i
           l = 2 * i + 1
           r = 2 * i + 2
       
           if l < n and arr[i][4] > arr[l][4]:
               largest = l
       
           if r < n and arr[largest][4] > arr[r][4]:
               largest = r
       
           # If root is not largest, swap with largest and continue heapifying
           if largest != i:
               arr[i], arr[largest] = arr[largest], arr[i]
               self. heapifyPD(arr, n, largest)
       
    def heapSortPD(self,arr):
           n = len(arr)
       
           # Build max heap
           for i in range(n//2, -1, -1):
               self.heapifyPD(arr, n, i)
       
           for i in range(n-1, 0, -1):
               # Swap
               arr[i], arr[0] = arr[0], arr[i]
       
               # Heapify root element
               self.heapifyPD(arr, i, 0)
           self.loaddata(arr)
           
    def heapifyIA(self,arr, n, i):
              # Find largest among root and children
              largest = i
              l = 2 * i + 1
              r = 2 * i + 2
          
              if l < n and arr[i][5] < arr[l][5]:
                  largest = l
          
              if r < n and arr[largest][5] < arr[r][5]:
                  largest = r
          
              # If root is not largest, swap with largest and continue heapifying
              if largest != i:
                  arr[i], arr[largest] = arr[largest], arr[i]
                  self. heapifyIA(arr, n, largest)
                  
          
          
    def heapSortIA(self,arr):
              n = len(arr)
          
              # Build max heap
              for i in range(n//2, -1, -1):
                  self.heapifyIA(arr, n, i)
          
              for i in range(n-1, 0, -1):
                  # Swap
                  arr[i], arr[0] = arr[0], arr[i]
          
                  # Heapify root element
                  self.heapifyIA(arr, i, 0)
              self.loaddata(arr)
              
    def heapifyID(self,arr, n, i):
               # Find largest among root and children
               largest = i
               l = 2 * i + 1
               r = 2 * i + 2
           
               if l < n and arr[i][5] > arr[l][5]:
                   largest = l
           
               if r < n and arr[largest][5] > arr[r][5]:
                   largest = r
           
               # If root is not largest, swap with largest and continue heapifying
               if largest != i:
                   arr[i], arr[largest] = arr[largest], arr[i]
                   self. heapifyID(arr, n, largest)
           
    def heapSortID(self,arr):
               n = len(arr)
           
               # Build max heap
               for i in range(n//2, -1, -1):
                   self.heapifyID(arr, n, i)
           
               for i in range(n-1, 0, -1):
                   # Swap
                   arr[i], arr[0] = arr[0], arr[i]
           
                   # Heapify root element
                   self.heapifyID(arr, i, 0)
               self.loaddata(arr)
               
               
    def heapifyMtA(self,arr, n, i):
              # Find largest among root and children
              largest = i
              l = 2 * i + 1
              r = 2 * i + 2
          
              if l < n and arr[i][6] < arr[l][6]:
                  largest = l
          
              if r < n and arr[largest][6] < arr[r][6]:
                  largest = r
          
              # If root is not largest, swap with largest and continue heapifying
              if largest != i:
                  arr[i], arr[largest] = arr[largest], arr[i]
                  self. heapifyMtA(arr, n, largest)
                  
          
          
    def heapSortMtA(self,arr):
              n = len(arr)
          
              # Build max heap
              for i in range(n//2, -1, -1):
                  self.heapifyMtA(arr, n, i)
          
              for i in range(n-1, 0, -1):
                  # Swap
                  arr[i], arr[0] = arr[0], arr[i]
          
                  # Heapify root element
                  self.heapifyMtA(arr, i, 0)
              self.loaddata(arr)
              
    def heapifyMtD(self,arr, n, i):
               # Find largest among root and children
               largest = i
               l = 2 * i + 1
               r = 2 * i + 2
           
               if l < n and arr[i][6] > arr[l][6]:
                   largest = l
           
               if r < n and arr[largest][6] > arr[r][6]:
                   largest = r
           
               # If root is not largest, swap with largest and continue heapifying
               if largest != i:
                   arr[i], arr[largest] = arr[largest], arr[i]
                   self. heapifyMtD(arr, n, largest)
           
    def heapSortMtD(self,arr):
               n = len(arr)
           
               # Build max heap
               for i in range(n//2, -1, -1):
                   self.heapifyMtD(arr, n, i)
           
               for i in range(n-1, 0, -1):
                   # Swap
                   arr[i], arr[0] = arr[0], arr[i]
           
                   # Heapify root element
                   self.heapifyMtD(arr, i, 0)
               self.loaddata(arr)
               
    def heapifyDA(self,arr, n, i):
              # Find largest among root and children
              largest = i
              l = 2 * i + 1
              r = 2 * i + 2
          
              if l < n and arr[i][7] < arr[l][7]:
                  largest = l
          
              if r < n and arr[largest][7] < arr[r][7]:
                  largest = r
          
              # If root is not largest, swap with largest and continue heapifying
              if largest != i:
                  arr[i], arr[largest] = arr[largest], arr[i]
                  self. heapifyDA(arr, n, largest)
                  
          
          
    def heapSortDA(self,arr):
              n = len(arr)
          
              # Build max heap
              for i in range(n//2, -1, -1):
                  self.heapifyMtA(arr, n, i)
          
              for i in range(n-1, 0, -1):
                  # Swap
                  arr[i], arr[0] = arr[0], arr[i]
          
                  # Heapify root element
                  self.heapifyDA(arr, i, 0)
              self.loaddata(arr)
              
    def heapifyDD(self,arr, n, i):
               # Find largest among root and children
               largest = i
               l = 2 * i + 1
               r = 2 * i + 2
           
               if l < n and arr[i][7] > arr[l][7]:
                   largest = l
           
               if r < n and arr[largest][7] > arr[r][7]:
                   largest = r
           
               # If root is not largest, swap with largest and continue heapifying
               if largest != i:
                   arr[i], arr[largest] = arr[largest], arr[i]
                   self. heapifyDD(arr, n, largest)
           
    def heapSortDD(self,arr):
               n = len(arr)
           
               # Build max heap
               for i in range(n//2, -1, -1):
                   self.heapifyDD(arr, n, i)
           
               for i in range(n-1, 0, -1):
                   # Swap
                   arr[i], arr[0] = arr[0], arr[i]
           
                   # Heapify root element
                   self.heapifyDD(arr, i, 0)
               self.loaddata(arr)
               
    def bucketSortWithIMBDA(self,array):
        bucket = []

        # Create empty buckets
        for i in range(len(array)):
            bucket.append([])

        # Insert elements into their respective buckets
        for j in array:
            index_b = int(10 * float(j[5]))
            bucket[index_b].append(j)

        # Sort the elements of each bucket
        for i in range(len(array)):
            bucket[i] = sorted(bucket[i])

        # Get the sorted elements
        k = 0
        for i in range(len(array)):
            for j in range(len(bucket[i])):
                array[k] = bucket[i][j]
                k += 1
        self.loaddata(array)
    def bucketSortWithRankA(self,array):
        bucket = []

        # Create empty buckets
        for i in range(len(array)):
            bucket.append([])

        # Insert elements into their respective buckets
        for j in array:
            index_b = int(10 * float(j[0]))
            bucket[index_b].append(j)

        # Sort the elements of each bucket
        for i in range(len(array)):
            bucket[i] = sorted(bucket[i])

        # Get the sorted elements
        k = 0
        for i in range(len(array)):
            for j in range(len(bucket[i])):
                array[k] = bucket[i][j]
                k += 1
        self.loaddata(array)

    
    #---CRUD Operations---#
    def fill_values(self):
        row = self.MoviesTable.currentRow()
        self.RegEdit.setText(self.MoviesTable.item(row,1).text())
        
    def resetValues(self):
        self.RegEdit.setText("Movie")
    
    def Delete_Movie(self):
        if(self.RegEdit.text() != "Movie"):
            ID = self.RegEdit.text()
            movie_found = False
            updated_data = []
            with open('products.csv', "r", encoding="utf-8") as fileInput:
                reader = csv.reader(fileInput)
                for row in reader:
                    if len(row) > 0:
                        if ID != row[1]:
                            updated_data.append(row)
                        else:
                            movie_found = True
            

            if movie_found is True:
                with open('products.csv', "w", encoding="utf-8", newline="") as fileInput:
                    writer = csv.writer(fileInput)
                    writer.writerows(updated_data)
                with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
                    d = list(csv.reader(fileInput))
                    d.remove(d[0])
                data.clear()
                for i in d:
                    data.append(i)
                self.loaddata(data)
                self.resetValues()
                
        
# main
app = QApplication(sys.argv)
wel = welcomepg()
widget = QtWidgets.QStackedWidget()
widget.addWidget(wel)
widget.setFixedHeight(500)
widget.setFixedWidth(800)
widget.show()
try:
    sys.exit(app.exec_())
except:
    print("Exiting")