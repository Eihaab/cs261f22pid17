# -*- coding: utf-8 -*-
"""
Created on Sat Oct 15 09:14:10 2022

@author: Admin
"""

import matplotlib.pyplot as plt
import pandas as pd

path=r'C:\SMESTER3\WebScrapping\DataFile.csv'
df=pd.read_csv(path)

list1,list2 = [],[]
movie,year,genre,rating,direction = [],[],[],[],[]
#print(df.head())
m=df['Movie Name']
y=df['Year']
g=df['Genre']
r=df['Rating']
d=df['Direction']
 
length = len(r)
def bubbleSort(data):
    length = len(data)
    # loop to access each data element
    for i in range(length):

        # loop to compare data elements
        for j in range(0, length - i - 1):
              
            # compare two adjacent elements
            # change > to < to sort in descending order
            if r[j] > r[j + 1]:

                # swapping elements if elements
                # are not in the intended order
                temp = df[j]
                df[j] = df[j+1]
                df[j+1] = temp

df = pd.DataFrame({'Movie Name':m,'Year':y,'Genre':g,'Rating':r,'Direction':d})
df.to_csv('DataFile.csv',index=False,encoding='utf-8')
print(df)

