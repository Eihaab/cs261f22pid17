# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 22:02:38 2022

@author: Admin
"""
import csv

rat,m,y,g,d = [],[],[],[],[]
with open('DataFile.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
for i in data:
    rat.append(float(i[3]))
    m.append(i[0])
    y.append(i[1])
    g.append(i[2])
    d.append(i[4])
# Quick sort in Python

# function to find the partition position
def partition(array, low, high):

  # choose the rightmost element as pivot
  pivot = array[high]

  # pointer for greater element
  i = low - 1

  # traverse through all elements
  # compare each element with pivot
  for j in range(low, high):
    if array[j] <= pivot:
      # if element smaller than pivot is found
      # swap it with the greater element pointed by i
      i = i + 1

      # swapping element at i with element at j
      (array[i], array[j],m[i],m[j],y[i],y[j],g[i],g[j],d[i],d[j]) = (array[j], array[i],m[j],m[i],y[j],y[i],g[j],g[i],d[j],d[i])

  # swap the pivot element with the greater element specified by i
  (array[i + 1], array[high],m[i+1],m[high],y[i+1],y[high],g[i+1],g[high],d[i+1],d[high]) = (array[high], array[i + 1],m[high],m[i + 1],y[high],y[i + 1],g[high],g[i + 1],d[high],d[i + 1])

  # return the position from where partition is done
  return i + 1

# function to perform quicksort
def quickSort(array, low, high):
  if low < high:

    # find pivot element such that
    # element smaller than pivot are on the left
    # element greater than pivot are on the right
    pi = partition(array, low, high)

    # recursive call on the left of pivot
    quickSort(array, low, pi - 1)

    # recursive call on the right of pivot
    quickSort(array, pi + 1, high)
  data1 = []
  for i in range(len(rat)):
      newD = []
      newD.append(m[i])
      newD.append(y[i])
      newD.append(g[i])
      newD.append(str(rat[i]))
      newD.append(d[i])
      data1.append(newD)
  print(data1)


size = len(rat)

quickSort(rat, 0, size - 1)
