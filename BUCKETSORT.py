# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 22:37:31 2022

@author: Admin
"""
import csv

with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])
# Bucket Sort in Python


def bucketSort(array):
    bucket = []

    # Create empty buckets
    for i in range(len(array)):
        bucket.append([])

    # Insert elements into their respective buckets
    for j in array:
        index_b = int(10 * float(j[5]))
        bucket[index_b].append(j)

    # Sort the elements of each bucket
    for i in range(len(array)):
        bucket[i] = sorted(bucket[i])

    # Get the sorted elements
    k = 0
    for i in range(len(array)):
        for j in range(len(bucket[i])):
            array[k] = bucket[i][j]
            k += 1
    return array


array = [.42, .32, .33, .52, .37, .47, .51]
print("Sorted Array in descending order is")
print(bucketSort(data))