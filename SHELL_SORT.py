# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 22:53:07 2022

@author: Admin
"""
import csv
with open('products.csv', 'r',encoding="utf-8",newline="") as fileInput:
    data = list(csv.reader(fileInput))
data.remove(data[0])

# Shell sort in python


def shellSort(array, n):

    # Rearrange elements at each n/2, n/4, n/8, ... intervals
    interval = n // 2
    while interval > 0:
        for i in range(interval, n):
            temp = array[i]
            j = i
            while j >= interval and array[j - interval] [5]> temp[5]:
                array[j] = array[j - interval]
                j -= interval

            array[j] = temp
        interval //= 2


#data = [9, 8, 3, 7, 5, 6, 4, 1]
size = len(data)
shellSort(data, size)
print('Sorted Array in Ascending Order:')
print(data)