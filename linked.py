
from bs4 import BeautifulSoup
import requests, openpyxl
from urllib.parse import urljoin
  


url='https://www.imdb.com/search/title/?title_type=feature&release_date=1950-01-01,2000-12-31&after=WzY0MjY1LCJ0dDAxODE5NDciLDM3MDUxXQ%3D%3D&ref_=adv_nxt'
while True:
    print(url)
    source=requests.get(url)
    soup=BeautifulSoup(source.text,'html.parser')
    tiles=soup.select_one('div.desc')
    print(tiles.span.text)
    nextpage=soup.select_one('div.desc>a')
    if nextpage:
        nexturl=nextpage.get('href')
        url=urljoin(url, nexturl)
    else:
        break 